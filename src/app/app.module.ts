import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { LoopsComponent } from './loops/loops.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { LayoutComponent } from './layout/layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AccordionTestComponent } from './accordion-test/accordion-test.component';
import { OutputTestComponent } from './output-test/output-test.component';

import { DomTestComponent } from './dom-test/dom-test.component';

@NgModule({
  declarations: [
    AppComponent, TestComponent, LoopsComponent, LayoutComponent, AccordionTestComponent, OutputTestComponent, DomTestComponent
  ],
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule, AccordionModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
