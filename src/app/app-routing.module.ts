import { NgModule } from "@angular/core";
import { Router } from "@angular/router";

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

const appRoutes: Routes = [
    { path: 'about', component: AboutComponent },
];

export class AppRoutingComponent {

}