import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";

@Component({
    selector: 'app-domtest',
    templateUrl: './dom-test.component.html'
})

export class DomTestComponent implements AfterViewInit {

    @ViewChild('testelement') testelement! : ElementRef;
    ngAfterViewInit() {
        console.log(this.testelement.nativeElement.innerHTML);
    }

}