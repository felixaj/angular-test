import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accordion-test',
  templateUrl: './accordion-test.component.html',
  styleUrls: ['./accordion-test.component.scss']
})
export class AccordionTestComponent implements OnInit {

  @Input() title1 = '';

  oneAtATime = true;

  constructor() { }

  ngOnInit(): void {
  }

}
