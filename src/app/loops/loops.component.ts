import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loops',
  templateUrl: './loops.component.html',
  styleUrls: ['./loops.component.scss']
})
export class LoopsComponent implements OnInit {

  x = [5, 6, 7, 8];

  constructor() { }

  ngOnInit(): void {
  }

}
