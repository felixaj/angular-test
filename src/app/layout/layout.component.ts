import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  testtitle1 = "Test title 1"

  constructor() { }

  ngOnInit(): void {
  }

  updateTitle(newTitle: string) {
    this.testtitle1 = newTitle;
  }

}
